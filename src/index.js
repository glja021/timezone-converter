const express = require('express');
const bodyParser = require('body-parser');
const compression = require('compression');
const cors = require('cors');
const package = require('../package.json');
const conversion = require('../api/conversion');
const timezones = require('../api/timezones');

const app = express();
const port = process.env.PORT || 3000;

app.use(cors());
app.use(compression());
app.use(bodyParser.json());

app.get('/api', (_,res) => res.json({ version: package.version }));
app.get('/api/timezones', timezones);
app.get('/api/conversion', conversion);

app.listen(port, () => 
    console.log(`Timezone converter listening over port ${port} for environment ${process.env.NODE_ENV || 'development'}, version (${package.version})`));