const moment = require('moment-timezone');
module.exports = (req,res) => {
    const timezoneOrUtc = (tz) => (moment.tz.zone(tz || 'GMT') || moment.tz.zone('GMT')).name;
    let date = moment.tz(req.query.datetime || moment.utc(), timezoneOrUtc(req.query.from));
    if(!date.isValid()){
        if(Number(req.query.datetime)){
            date = moment(Number(req.query.datetime)).tz(timezoneOrUtc(req.query.from))
        } else {
            res
            .status(400)
            .json('datetime invalid');
            return;
        }
    }
    const converted = date.clone().tz(timezoneOrUtc(req.query.to));
    res.json({
        unixTimestamp: date.format('x'), 
        from: {
            datetime: date.format(req.query.format),
            timezone: date.tz(),
            abbreviation: date.format('z'),
            offset: date.format('Z'),
        }, 
        to:{
            
            datetime: converted.format(req.query.format),
            timezone: converted.tz(),
            abbreviation: converted.format('z'),
            offset: converted.format('Z'),
        }
    });
};