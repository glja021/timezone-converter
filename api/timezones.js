const moment = require('moment-timezone');
module.exports = (req,res) =>{
    res.json(moment.tz.names());
};